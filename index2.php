<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Kecamatan Coblong</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<!-- css -->
<link href="css/bootstrap.css" rel="stylesheet" />
<link href="css/portal.css" rel="stylesheet" />

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body style="  background-color: #F2F2F2;">
	<header class="" style ="border-bottom: 10px solid #000; background-color: #C1C1C1;"></center>
		<center><img src="img/logo-new.png" style="background-color: #C1C1C1; width: 100%; height: 250px; "></center>
	</header>
	
	<div class="container">
	    <div class="col-md-2 col-md-offset-1">
	    	<div class="menu-head" style="background-color: #C00000;">
	    		<center><strong><p>ePemerintahan</p></strong></center>
	    	</div>
	    	<div align="center" class="menu" style="background-color: #FF0000;">
					<a href="website.php">
						<img src="img/portal/portal.epemerintahan.png"/>
					</a>
			</div>
	    </div>
	    <div class="col-md-2">
	    	<div class="menu-head" style="background-color: #E73C0B;">
	    		<center><strong><p>Agenda<br>Nusantara</p></strong></center>
	    	</div>
	    	<div align="center" class="menu" style="background-color: #F79646;">
					<a href="berita.php">
						<img src="img/portal/portal.agenda-nusantara.png"/>
					</a>
			</div>
		</div>
	    <div class="col-md-2">
	    	<div class="menu-head" style="background-color: #0A83B0;">
	    		<center><strong><p>Nusantara<br>Updates</p></strong></center>
	    	</div>
	    	<div align="center" class="menu" style="background-color: #00B0F0;">
					<a href="pelayanan.php">
						<img src="img/portal/portal.nusantara-updates.png"/>
					</a>
			</div>
	    </div>
	    <div class="col-md-2">
	    	<div class="menu-head" style="background-color: #77CC1D;">
	    		<center><strong><p>Belanja<br>Nusantara</p></strong></center>
	    	</div>
	    	<div align="center" class="menu" style="background-color: #92D050;">
					<a href="#">
						<img src="img/portal/portal.belanja-nusantara.png"/>
					</a>
			</div>
	    </div>
	    <div class="col-md-2">
	    	<div class="menu-head" style="background-color: #CCA300;">
	    		<center><strong><p>Community<br>Channel</p></strong></center>
	    	</div>
	    	<div align="center" class="menu" style="background-color: #FFCC00;">
					<a href="http://e-pemerintahan.com/home/cek_surat">
						<img src="img/portal/portal.community-channel.png"/>
					</a>
				</div>
	    </div>
	</div>
	
	<footer class="footerline-services">
			<p align="center"><i>&copy; 2015 - e-Pemerintahan.com</i></p>
	</footer>
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
</body>
</html>
